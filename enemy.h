#pragma once
#include "character.h"

using namespace std;

class enemy : public character {
public:
	enemy(string inputedName, string inputedBio) : character(inputedName, inputedBio) {};
	void fight(character *other);
	bool isAlive();
	void setStats(int level);
};