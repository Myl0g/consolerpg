#include "character.h"
using namespace std;

character::character() : character::character("Default Name", "Default Description") {};

character::character(string inputedName, string inputedBio)
{
	id = inputedName;
	bio = inputedBio;
}
void character::introduce()
{
	cout << id << " " << bio << endl;
}
