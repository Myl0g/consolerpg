#pragma once
#include "character.h"

using namespace std;

class player : public character {
public:
	player(string inputedName, string inputedBio) : character(inputedName, inputedBio) {};
	void fight(character *other);
	bool isAlive();
	void setStats(int level);
	int potion = 3;

};