PREFIX = /usr

CXX = g++
CXXFLAGS = -I.

DEPS = utilities.h character.h enemy.h player.h 
OBJ = character.o enemy.o player.o main.o

all: build

%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

build: $(OBJ)
	$(CXX) -o consolerpg character.o enemy.o player.o main.o

run: build
	./consolerpg

install: build
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp ./consolerpg $(DESTDIR)$(PREFIX)/bin

clean:
	rm ./*.o
	rm ./consolerpg